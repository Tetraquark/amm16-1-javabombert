package amm16_javabombert.entity;

public abstract class MapTile extends GameObject implements Paintable{
    
	public enum TileType{
    	BLOCKER,			// блокирующий тайл карты
    	PERMEABLE			// проницаемый тайл карты
    }
	
	private TileType tileType;
	
	public MapTile(int x, int y, int tileWidth, int tileHeigth, TileType type){
		setX(x);
		setY(y);
		setType(type);
		setWidth(tileWidth);
		setHeight(tileHeigth);
	}
	
	public void setType(TileType type){
		this.tileType = type;
	}
	
	public TileType getType(){
		return this.tileType;
	}
	
}

package amm16_javabombert.entity;

/**
 *
 * Направления движения игровых объектов.
 *
 */
public enum MoveDirection {
    STAY,
    UP,
    RIGHT,
    DOWN,
    LEFT,
}

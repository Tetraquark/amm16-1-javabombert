package amm16_javabombert.entity;

import amm16_javabombert.core.Sprite;
import java.awt.Graphics2D;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "bullet")
public class Bullet extends GameObject implements Paintable, Movable{
    private MoveDirection direction;
    private static int speed;
    private int doDamageFromHit;
    private int shotPlayerID;
    
    @XmlTransient
    private Sprite sprite;
    
    public Bullet(){
    	super();
    }
    
    @Override
    public void onPaint(Graphics2D g){
    	sprite.setCoords(getX(), getY());
    	sprite.drawSprite(g);
    }

    @Override
    public void onMove(){
                switch(direction){
            case LEFT:
                setX(getX() - speed);
                break;
            
            case RIGHT:
                setX(getX() + speed);
                break;
                
            case UP:
                setY(getY() - speed);
                break;
            
            case DOWN:
                setY(getY() + speed);
                break;
            default:
                System.out.println("Bullet: " + getIdent() + ". Direction doesn't assigned yet");
                break;
        }
    }

    public MoveDirection getDirection(){
        return direction;
    }

    public void setDirection(MoveDirection direction) {
        this.direction = direction;
    }

    public static int getSpeed() {
        return speed;
    }

    public static void setSpeed(int speed) {
        Bullet.speed = speed;
    }

    public int getDoDamageFromHit() {
        return doDamageFromHit;
    }

    public void setDoDamageFromHit(int doDamageFromHit) {
        this.doDamageFromHit = doDamageFromHit;
    }

    public int getShotPlayerID() {
        return shotPlayerID;
    }

    public void setShotPlayerID(int shotPlayerID) {
        this.shotPlayerID = shotPlayerID;
    }
    
    @Override
    public void setSprite(Sprite sprite){
    	this.sprite = sprite;
    }
    
    @Override
    public Sprite getSprite(){
    	return sprite;
    }

	@Override
	public int getNextX(){
		int xDirectCoef = 0;
		if(direction == MoveDirection.LEFT)
			xDirectCoef = -1;
		if(direction == MoveDirection.RIGHT)
			xDirectCoef = 1;
			
		return getX() + xDirectCoef * speed;
	}

	@Override
	public int getNextY(){
		int yDirectCoef = 0;
		if(direction == MoveDirection.DOWN)
			yDirectCoef = 1;
		if(direction == MoveDirection.UP)
			yDirectCoef = -1;
			
		return getY() + yDirectCoef * speed;
	}
}

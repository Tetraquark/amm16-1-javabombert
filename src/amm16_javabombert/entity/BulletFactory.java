package amm16_javabombert.entity;

/**
 *
 * Фабрика снарядов танков
 *
 */
public class BulletFactory implements GameObjectsFactory {

    @Override
    public GameObject createGameObject() {
        return new Bullet();
    }

}

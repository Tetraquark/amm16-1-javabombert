package amm16_javabombert.entity;

import java.awt.Graphics2D;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import amm16_javabombert.core.Sprite;

@XmlRootElement(name = "tilefloor")
public class TileFloor extends MapTile{
	
	@XmlTransient
	private Sprite sprite;
	
	public TileFloor(int x, int y, int tileWidth, int tileHeight){
		super(x, y, tileWidth, tileHeight, TileType.PERMEABLE);
	}
	
	@Override
	public void onPaint(Graphics2D g){
    	sprite.setCoords(getX(), getY());
    	sprite.drawSprite(g);
	}

	@Override
	public void setSprite(Sprite sprite){
		this.sprite = sprite;
	}

	@Override
	public Sprite getSprite(){
		return sprite;
	}

}

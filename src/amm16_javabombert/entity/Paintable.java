package amm16_javabombert.entity;

import amm16_javabombert.core.Sprite;
import java.awt.Graphics2D;

public interface Paintable{
	public void setSprite(Sprite sprite);
	public Sprite getSprite();
    public void onPaint(Graphics2D g);
}

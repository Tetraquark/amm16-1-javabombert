package amm16_javabombert.entity;

public interface Movable {

    public void onMove();
    public int getNextX();
    public int getNextY();
}

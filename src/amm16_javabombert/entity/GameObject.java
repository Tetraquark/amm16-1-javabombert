package amm16_javabombert.entity;

import java.awt.Rectangle;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 *
 * Абстрактный класс игровых сущностей.
 *
 */
@XmlSeeAlso({Wall.class, Tank.class, Bullet.class, TileFloor.class})
public abstract class GameObject {

    private int x;
    private int y;
    private int ident;
    private int width;
    private int height;
    
    private Rectangle objRect;
    
    public GameObject(){
    	objRect = new Rectangle();
    }
    
    public int getX(){
        return x;
    }

    public void setX(int x) {
        this.x = x;
        this.objRect.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
        this.objRect.y = y;
    }

    public int getIdent() {
        return ident;
    }

    public void setIdent(int ident) {
        this.ident = ident;
    }

	public int getWidth(){
		return width;
	}

	public void setWidth(int width){
		this.width = width;
		this.objRect.width = width;
	}

	public int getHeight(){
		return height;
	}

	public void setHeight(int height){
		this.height = height;
		this.objRect.height = height;
	}
	
	public Rectangle getRect(){
		return objRect;
	}
	
	/**
	 * Проверка на коллизии двух объектов.
	 * @param obj ссылка на игровой объект GameObject
	 * @return true - есть коллизия, false - нет коллизии.
	 */
	public boolean checkColl(GameObject obj){
		return objRect.intersects(obj.getRect());
	}
	
	@Override
	public String toString(){
		return "x = " + getX() + "; y = " + getY() + "; w = " + getWidth() + "; h = " + getWidth() + ";";
	}
}

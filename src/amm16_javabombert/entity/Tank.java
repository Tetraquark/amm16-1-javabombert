package amm16_javabombert.entity;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import amm16_javabombert.core.Sprite;
import java.awt.Graphics2D;

@XmlRootElement(name = "tank")
public class Tank extends GameObject implements Paintable, Movable {

    private MoveDirection direction;
    private static int hitpoints;
    private boolean isMoving;
    private int speed = 0;
    @XmlTransient
    private Sprite sprite;
    
    Tank(){}
    
    @Override
    public void onPaint(Graphics2D g){
    	sprite.setCoords(getX(), getY());
    	sprite.drawSprite(g);
    }

    @Override
    public void onMove(){
        switch(direction){
            case LEFT:
                setX(getX() - speed);
                break;
            
            case RIGHT:
                setX(getX() + speed);
                break;
                
            case UP:
                setY(getY() - speed);
                break;
            
            case DOWN:
                setY(getY() + speed);
                break;
            default:
                System.out.println("Tank: " + getIdent() + ". Direction doesn't assigned yet");
                break;
        }
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
    
    public static int getHitpoints() {
        return hitpoints;
    }

    public static void setHitpoints(int hitpoints) {
        Tank.hitpoints = hitpoints;
    }

    public MoveDirection getDirection() {
        return direction;
    }

    public void setDirection(MoveDirection direction) {
        this.direction = direction;
        sprite.setDirection(direction);
    }

    public boolean isIsMoving() {
        return isMoving;
    }

    public void setIsMoving(boolean isMoving) {
        this.isMoving = isMoving;
    }
    
    @Override
    public void setSprite(Sprite sprite){
    	this.sprite = sprite;
    }
    
    @Override
    public Sprite getSprite(){
    	return sprite;
    }

	@Override
	public int getNextX(){
		int xDirectCoef = 0;
		if(direction == MoveDirection.LEFT)
			xDirectCoef = -1;
		if(direction == MoveDirection.RIGHT)
			xDirectCoef = 1;
			
		return getX() + xDirectCoef * speed;
	}

	@Override
	public int getNextY(){
		int yDirectCoef = 0;
		if(direction == MoveDirection.DOWN)
			yDirectCoef = 1;
		if(direction == MoveDirection.UP)
			yDirectCoef = -1;
			
		return getY() + yDirectCoef * speed;
	}
}

package amm16_javabombert.entity;

/**
 *
 * Интерфейс для фабрик объектов класса GameObject.
 *
 */
public interface GameObjectsFactory {

    public GameObject createGameObject();

}

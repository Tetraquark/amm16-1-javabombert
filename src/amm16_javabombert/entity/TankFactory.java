package amm16_javabombert.entity;

/**
 *
 * Фабрика танков
 *
 */
public class TankFactory implements GameObjectsFactory{

	@Override
	public GameObject createGameObject(){
		return new Tank();
	}

}

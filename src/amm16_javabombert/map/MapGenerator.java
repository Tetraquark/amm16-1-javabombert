package amm16_javabombert.map;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import amm16_javabombert.entity.MapTile;
import amm16_javabombert.entity.TileFloor;
import amm16_javabombert.entity.Wall;

public class MapGenerator{
	
	/*
	final static private int cellWidth = 10, cellHeight = 8;
	final static private int ROOM_MIN = 5;
	
	private int cellsX;
	private int cellsY;
	private double prob = 0.65;
	*/
	
	/**
	 * Метод генерации карты
	 * @param width ширина карты в тайлах
	 * @param height высота карты в тайлах
	 * @return сгенерированная карты 
	 */
	public GameMap generateMap(int width, int height){
		GameMap genMap = new GameMap(width, height);
		
		return genMap;
	}
	
	/**
	 * Загрузка карты из файла.
	 * @param fileName имя файла
	 * @param tileWidthPix размер ширины тайла в пикселях
	 * @param tileHeightPix размер высоты тайла в пикселях
	 * @return объект Map, представляющий карту
	 * @throws IOException в случае ошибки при открытии карты или чтении из файла.
	 */
	public GameMap loadMapFromFile(String fileName, int tileWidthPix, int tileHeightPix) throws IOException{
		int mapTilesWidth = 0, mapTilesHeight = 0;
		
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		
		String line = null;
		line = reader.readLine();
		if(line != null){
			mapTilesWidth = Integer.parseInt(line);
		}
		line = reader.readLine();
		if(line != null){
			mapTilesHeight = Integer.parseInt(line);
		}
		
		GameMap map = new GameMap(mapTilesWidth, mapTilesHeight);
		int mapHeightCounter = 0;
		while((line = reader.readLine()) != null){
			String[] tilesLine = line.split(";");
			for(int i = 0; i < mapTilesWidth; i++){
				int tileCode = Integer.parseInt(tilesLine[i]);
				MapTile newMapTile = null;;
				switch(tileCode){
					case 1:{
						newMapTile = new Wall(i * tileWidthPix, mapHeightCounter * tileHeightPix, tileWidthPix, tileHeightPix);
						break;
					}
					case 0:{
						newMapTile = new TileFloor(i * tileWidthPix, mapHeightCounter * tileHeightPix, tileWidthPix, tileHeightPix);
						break;
					}
				}
				map.addMapTile(newMapTile);
			}
			mapHeightCounter++;
		}
		
		reader.close();
		
		return map;
	}
	
	/*
	public void create(){
        List<Room> rooms = generateRooms();
        copyListToArray(rooms);
        connectRooms(rooms);
	}
	
    private List<Room> generateRooms(int mapWidth, int mapHeight){
        List<Room> rooms = new ArrayList<Room>(mapWidth * mapHeight);
        Random rand = new Random();
        int roomWidth, roomHeight;
        for(int y = 0; y < cellsY; y++){
            for(int x = 0; x < cellsX; x++){
                if(rand.nextDouble() < prob) {
                    roomWidth = ROOM_MIN + rand.nextInt(cellWidth - 6);
                    roomHeight = ROOM_MIN + rand.nextInt(cellHeight - 6);
                    rooms.add(new Room(x * cellWidth, y * cellHeight, roomWidth, roomHeight));
                }
            }
        }

        return rooms;
    }
    
    private void copyListToArray(List<Room> rooms){
        for(Room room : rooms) {
            for (int y = room.getY()+1; y < room.getY() + room.getHeight()-1; y++) {
                for(int x = room.getX()+1; x < room.getX() + room.getWidth()-1; x++){
                	map.set(x, y, Floor);
                }
            }
        }
    }
	
    private void connectRooms(List<Room> rooms){
        Room first = rooms.remove(0);
        Room next;
        while(rooms.size() > 0){
            next = getClosestRoom(first, rooms);
            addConnections(first, next);
            first = next;
        }
    }
    
    private void addConnections (Room first, Room second){
        int firstX = first.centerX();
        int secondX = second.centerX();
        int firstY = first.centerY();
        int secondY = second.centerY();

        while(firstX != secondX){
            if(firstX < secondX){
                firstX++;
            }
            else{
                firstX--;
            }
            map.set(firstX, firstY, Floor);
        }

        while(firstY != secondY){
            if(firstY < secondY){
                firstY++;
            }
            else{
                firstY--;
            }
            map.set(firstX, firstY, Floor);
        }

    }

    private Room getClosestRoom(Room room, List<Room> rooms){
        int curPos = -1;
        int curDis = Integer.MAX_VALUE;

        Room current;
        int nextDis;
        for(int i = 0; i < rooms.size(); i++){
            current = rooms.get(i);
            nextDis = distance(room, current);
            if(nextDis < curDis){
                curPos = i;
                curDis = nextDis;
            }
        }

        return rooms.remove(curPos);
    }
    
    private int distance(Room roomOne, Room roomTwo){
        int xDis = roomOne.centerX() - roomTwo.centerX();
        xDis *= xDis;

        int yDis = roomOne.centerY() - roomTwo.centerY();
        yDis *= yDis;

        return (int)Math.sqrt(xDis + yDis);
    }
    
    private static class Room {
        int x, y, width, height;

        public Room(int x, int y, int width, int height){
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }

        public int centerX(){
            return x + (width / 2);
        }

        public int centerY(){
            return y + (height / 2);
        }
    }
    */
}

package amm16_javabombert.map;

import java.util.ArrayList;

import amm16_javabombert.entity.MapTile;

/**
 * 
 * Класс контейнер для объектов MapTile.
 * Представляет из себя сущность игровой карты.
 *
 */
public class GameMap{
	private ArrayList<MapTile> mapList;
	
	private int widthInTiles;
	private int heightInTiles;
	
	public GameMap(int widthInTiles, int heightInTiles){
		mapList = new ArrayList<MapTile>(widthInTiles * heightInTiles);
	}
	
	/**
	 * Добавить новый тайл карты.
	 * @param mapTile ссылка на объект тайла
	 * @return true - если добавление успешно иначе false 
	 */
	public boolean addMapTile(MapTile mapTile){
		mapList.add(mapTile);
		
		return true;
	}
	
	public int getWidthInTiles(){
		return widthInTiles;
	}

	public int getHeightInTiles(){
		return heightInTiles;
	}
	
	public ArrayList<MapTile> getTilesList(){
		return mapList;
	}
	
}

package amm16_javabombert.network;

import amm16_javabombert.core.ClientContext;
import amm16_javabombert.core.ClientMsg;
import amm16_javabombert.core.GameContext;
import amm16_javabombert.core.StateManager;
import static amm16_javabombert.network.NetworkManager.staticExitMsg;
import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;

public class ClientAgent extends AbstractAgent {
    private StateManager stateManager;
    
    public ClientAgent(Socket socket, List<AbstractAgent> connectedAbstractAgents) {
        super(socket, connectedAbstractAgents);
        stateManager = StateManager.getInstance();
    }

    public ClientAgent(String addr, int port, List<AbstractAgent> connectedAbstractAgents) {
        super(addr, port, connectedAbstractAgents);
        stateManager = StateManager.getInstance();
    }
    
    @Override
    public void run() {
        while(isThreadStart)
        {
            try {
                while (in.ready()) 
                {
                    String rcv_msg = in.readLine();
                    System.out.println(rcv_msg);
                    GameContext context = new GameContext();
                    context = context.deserialize(rcv_msg);
                    ((ClientContext)stateManager.getCurrentGameState()).setGameContext(context);
                }
                /* change current client context with reveived msg */
                /* stateManager.setContext(context) */
            } catch (IOException ex) {
                Logger.getLogger(AbstractAgent.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JAXBException ex) {
                Logger.getLogger(GameAgent.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void sendMsg(ClientMsg msg) throws JAXBException {
        if(isSocketOpen) {
            System.out.println(msg.serialize().toString());
            out.println(msg.serialize().toString());
        }
    }
    
    public void sendExit() {
        if(isSocketOpen) {
            out.println(staticExitMsg);
        }
    }
}

package amm16_javabombert.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractAgent extends Thread {

    public BufferedReader in;
    public PrintWriter out;
    public boolean isThreadStart, isSocketOpen;
    private Socket socket;
    private List<AbstractAgent> connectedAbstractAgents;

    public AbstractAgent(Socket socket, List<AbstractAgent> connectedAbstractAgents) {
        this.socket = socket;
        this.connectedAbstractAgents = connectedAbstractAgents;
        isSocketOpen = true;

        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
            close();
        }
        isThreadStart = false;
    }

    public AbstractAgent(String addr, int port, List<AbstractAgent> connectedAbstractAgents) {
        try {
            this.socket = new Socket(addr, port);
            isSocketOpen = true;
        } catch (IOException ex) {
            System.err.println("Error connecting to " + addr + ":" + port);
            isSocketOpen = false;
        }
        this.connectedAbstractAgents = connectedAbstractAgents;

        if(isSocketOpen) {
        try {
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);
            } catch (IOException e) {
                e.printStackTrace();
                close();
            }
        }
        isThreadStart = false;
    }
    
    public boolean startThread() {
        if(isSocketOpen) {
            isThreadStart = true;
            this.start();
            return true;
        } else {
            System.err.println("Cannot run client because of unconsistent connection state");
            return false;
        }
    }

    @Override
    public void run() {
        /* Ovverride in child obj*/
    }

    public void close() {
        try {
            in.close();
            out.close();
            socket.close();
            isSocketOpen = false;
        } catch (Exception e) {
            System.err.println("Exception");
        }
    }
}

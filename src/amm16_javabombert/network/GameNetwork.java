package amm16_javabombert.network;

import amm16_javabombert.core.GameContext;
import javax.xml.bind.JAXBException;

public class GameNetwork extends AbstractNetwork {
    public GameNetwork(){
        super(true);
    }
    
    public void sendAll(GameContext gc) throws JAXBException {
        String serialized = gc.serialize().toString();
        for(AbstractAgent i: connectedAgents) {
            i.out.println(serialized);
        }
    }
}

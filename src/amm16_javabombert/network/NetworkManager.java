package amm16_javabombert.network;

import amm16_javabombert.core.ContextHandler;
import amm16_javabombert.core.GameContext;
import amm16_javabombert.core.StateManager;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;

public final class NetworkManager {


    static int staticGamePort = 10888;
    static final String staticExitMsg = "hey_guys_iam_quit";

    private static volatile NetworkManager instance;
    String connect_ip_addr = "127.0.0.1";

    private GameNetwork gameNetwork;
    /* Если приложение -- сервер. Иначе null. */
    private ClientAgent clientAgent;
    /* Если приложение -- клиент. Иначе null. */

    private ContextHandler contextHandler;
    private boolean isServer;

    private NetworkManager() {
    }

    public static NetworkManager getInstance() {
        NetworkManager localInstance = instance;
        if (localInstance == null) {
            synchronized (NetworkManager.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new NetworkManager();
                }
            }
        }
        return localInstance;
    }
    
    public boolean setConnectIpAddr(String new_ip)
    {
        if(validIP(new_ip) == false)
            return false;
      
        connect_ip_addr = new_ip;
        return true;
    }
    
    public void setServerPort(int new_port)
    {
        staticGamePort = new_port;
        return;
    }

    public void setGameStateHandler() {
        contextHandler = StateManager.getInstance();
    }

    public void isNetworkAsServer(boolean isServer) {
        if (isServer) {
            gameNetwork = new GameNetwork();
            gameNetwork.listenerThreadStart();
        } else {
            Socket sock = null;
            try {
                sock = new Socket(connect_ip_addr, staticGamePort);
            } catch (IOException ex) {
                Logger.getLogger(NetworkManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            clientAgent = new ClientAgent(sock, null);
        }
        this.isServer = isServer;
    }

    public void sendGameContext() throws JAXBException {
        if (isServer) {
            GameContext gt = (GameContext)contextHandler.getContext();
            gameNetwork.sendAll(gt);
        }
    }
    
    public static boolean validIP (String ip) {
        try 
        {
            if ( ip == null || ip.isEmpty() ) 
            {
                return false;
            }

            String[] parts = ip.split( "\\." );
            if ( parts.length != 4 ) 
            {
                return false;
            }

            for ( String s : parts ) 
            {
                int i = Integer.parseInt( s );
                if ( (i < 0) || (i > 255) ) 
                {
                     return false;
                }
            }
        
            if ( ip.endsWith(".") ) 
            {
                return false;
            }

            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
}

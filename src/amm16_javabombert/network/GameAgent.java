package amm16_javabombert.network;

import amm16_javabombert.core.ClientMsg;
import amm16_javabombert.core.StateManager;
import static amm16_javabombert.network.NetworkManager.staticExitMsg;
import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;

public class GameAgent extends AbstractAgent {
    private StateManager stateManager;
    private boolean gameAgentIsRun = false;
    
    public GameAgent(Socket socket, List<AbstractAgent> connectedAbstractAgents) {
        super(socket, connectedAbstractAgents);
        stateManager = StateManager.getInstance();
    }
    
    @Override
    public void run() {
        gameAgentIsRun = true;
        while(gameAgentIsRun)
        {
            try {
                while (in.ready()) 
                {
                    String rcv_msg = in.readLine();
                    System.out.printf("Client rcv msg %s", rcv_msg);
                    if(rcv_msg.equals(staticExitMsg)) {
                        close();
                        gameAgentIsRun = false;
                        System.err.println("Client disconnected");
                        return;
                    }
                    ClientMsg msg = new ClientMsg();
                    msg = msg.deserialize(rcv_msg);
                    stateManager.putClientMsg(msg);
                }
            } catch (IOException ex) {
                Logger.getLogger(AbstractAgent.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JAXBException ex) {
                Logger.getLogger(GameAgent.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

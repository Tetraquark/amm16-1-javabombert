package amm16_javabombert.network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

abstract class AbstractNetwork extends Thread {

    private ServerSocket server;
    private boolean threadIsWorking;
    List<AbstractAgent> connectedAgents = Collections.synchronizedList(new ArrayList<AbstractAgent>());
    private boolean isGameServer = false;

    public AbstractNetwork(boolean isGameServer) {
        threadIsWorking = false;
        this.isGameServer = isGameServer;
        try {
            server = new ServerSocket(NetworkManager.staticGamePort);
        } catch (IOException ex) {
            Logger.getLogger(AbstractNetwork.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public AbstractNetwork(int port, boolean isGameServer) {
        threadIsWorking = false;
        this.isGameServer = isGameServer;
        try {
            server = new ServerSocket(port);
        } catch (IOException ex) {
            Logger.getLogger(AbstractNetwork.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void listenerThreadStart() {
        threadIsWorking = true;
        this.start();
    }

    public void run() {
        while (threadIsWorking) {
            try {
                Socket socket = server.accept();
                System.out.println(socket.getRemoteSocketAddress().toString());
                GameAgent agent = null;
                if(isGameServer) {
                    agent = new GameAgent(socket, connectedAgents);
                }
                else {
                    agent = null; // Stanislav implementation
                }
                connectedAgents.add(agent);
                agent.start();
            } catch (IOException ex) {
                Logger.getLogger(AbstractNetwork.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void closeAll() {
        try {
            server.close();
            synchronized (connectedAgents) {
                Iterator<AbstractAgent> iter = connectedAgents.iterator();
                while (iter.hasNext()) {
                    ((GameAgent) iter.next()).close();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(AbstractNetwork.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            connectedAgents.clear();
        }
    }
}

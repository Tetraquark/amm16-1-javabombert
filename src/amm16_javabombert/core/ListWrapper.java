package amm16_javabombert.core;

import amm16_javabombert.entity.GameObject;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElementWrapper;

public class ListWrapper {

    @XmlElementWrapper(name = "wrapperList")
    private List<GameObject> list;

    public ListWrapper() {
        list = new ArrayList<>();
    }

    public ListWrapper(ArrayList<GameObject> list) {
        this.list = list;
    }
    
    public void setList(List<GameObject> list) {
        this.list = list;
    }

    public List<GameObject> getList(){
    	return list;
    }
    
    public void add(GameObject o) {
        list.add(o);
    }
    
}

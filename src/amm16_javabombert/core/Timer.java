package amm16_javabombert.core;

/**
 *
 * Игровой таймер для ограничения FPS игрового цикла.
 *
 */
public class Timer {

    public long currentTime = System.nanoTime();
    public long deltaTime;
    public int deltaTimeMS;
    public long lastUpdateTime;

    public void update() {
        lastUpdateTime = currentTime;
        currentTime = System.nanoTime();
        deltaTime = currentTime - lastUpdateTime;
        deltaTimeMS = (int) (deltaTime / 1000000);
    }

    public int currFPSDuration() {
        return (int) (System.nanoTime() - currentTime) / 1000000;
    }
}

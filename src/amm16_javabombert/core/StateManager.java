package amm16_javabombert.core;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * Сиглетон
 *
 */
public final class StateManager implements KeyListener, ContextHandler{

    private static volatile StateManager instance;
    private AppState gameStateObj;
    private AppState.State currentState;

    private Map<AppState.State, AppState> statesMap; 
    private List<ClientMsg> clientMsgList = null;
    
    private StateManager(){}
    
    public static StateManager getInstance(){
        StateManager localInstance = instance;
        if(localInstance == null){
            synchronized (StateManager.class){
                localInstance = instance;
                if (localInstance == null){
                    instance = localInstance = new StateManager();
                }
            }
        }
        return localInstance;
    }

    public void init(){
        statesMap = new HashMap<AppState.State, AppState>();
        statesMap.put(AppState.State.MAINMENU_STATE, new MainMenuState());
        statesMap.put(AppState.State.CLIENT_STATE, new ClientContext()); //TODO: ResManager обернуть в интерфейс, прокинуть везде
        statesMap.put(AppState.State.SERVER_STATE, new ServerContext());
        
        (statesMap.get(AppState.State.MAINMENU_STATE)).setStateManager(instance);
        (statesMap.get(AppState.State.CLIENT_STATE)).setStateManager(instance);
        (statesMap.get(AppState.State.SERVER_STATE)).setStateManager(instance);
        
        gameStateObj = statesMap.get(AppState.State.MAINMENU_STATE);
	clientMsgList = Collections.synchronizedList(new ArrayList());
    }
    
    public synchronized void setCurrentState(AppState.State currentState){
    	if(this.currentState != currentState){
    		gameStateObj.dispose();
    		this.currentState = currentState;
            this.gameStateObj = statesMap.get(currentState);
            gameStateObj.init();
    	}
    }
    
    public void update(int dt){
    	if(gameStateObj != null)
    		gameStateObj.update(dt);
    }
    
    public void draw(Graphics2D g){
    	if(gameStateObj != null)
    		gameStateObj.draw(g);
    }

	@Override
	public void keyPressed(KeyEvent e){
		if(gameStateObj != null)
			gameStateObj.keyPressed(e);
	}

	@Override
	public void keyReleased(KeyEvent e){
		if(gameStateObj != null)
			gameStateObj.keyReleased(e);
	}

	@Override
	public void keyTyped(KeyEvent e){
		if(gameStateObj != null)
			gameStateObj.keyTyped(e);
	}

    @Override
	public Object getContext(){
		if(currentState == AppState.State.SERVER_STATE)
			return ((ServerContext) gameStateObj).getGameContext();
		else if(currentState == AppState.State.CLIENT_STATE)
			return ((ClientContext) gameStateObj).getGameContext();
		else
			return null;
	}
    
    public AppState getCurrentGameState(){
    	return gameStateObj;
    }
    
    public synchronized void putClientMsg(ClientMsg msg)
    {
        if (clientMsgList == null)
            clientMsgList = new ArrayList();
        clientMsgList.add(msg);
    }
    
    public boolean isEmptyCilentMsgs()
    {
        if (clientMsgList == null)
            clientMsgList = new ArrayList();
        return clientMsgList.isEmpty();
    }

    public List<ClientMsg> getClientMsgList() {
        return clientMsgList;
    }

    public void setClientMsgList(List<ClientMsg> clientMsgList) {
        this.clientMsgList = clientMsgList;
    }
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amm16_javabombert.core;

import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Александр
 */
@XmlRootElement
public class ClientMsg {
    
    @XmlElement
    public int a;
    @XmlElement
    public int b;
    @XmlElement
    public String c = "Test string";

    public String serialize() throws JAXBException {
        JAXBContext jc;
        StringWriter os;

        jc = JAXBContext.newInstance(ClientMsg.class);
        os = new StringWriter();
        jc.createMarshaller().marshal(this, os);

        return os.toString();
    }

    public ClientMsg deserialize(String s) throws JAXBException {
        ClientMsg de_context = new ClientMsg();
        StringReader sr = new StringReader(s);
        JAXBContext jc = JAXBContext.newInstance(ClientMsg.class);
        de_context = (ClientMsg) jc.createUnmarshaller().unmarshal(sr);

        return de_context;
    }
}

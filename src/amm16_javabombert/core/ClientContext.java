package amm16_javabombert.core;

import amm16_javabombert.entity.Bullet;
import amm16_javabombert.entity.GameObject;
import amm16_javabombert.entity.MapTile;
import amm16_javabombert.entity.MapTile.TileType;
import amm16_javabombert.entity.MoveDirection;
import amm16_javabombert.entity.Tank;
import amm16_javabombert.entity.TankFactory;
import amm16_javabombert.entity.TileFloor;
import amm16_javabombert.entity.Wall;
import amm16_javabombert.map.GameMap;
import amm16_javabombert.map.MapGenerator;
import javafx.scene.input.KeyCode;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

/**
 *
 * Контекст игры, стейт игрового процесса на клиентской стороне.
 *
 */
public class ClientContext extends AppState {

    private GameContext gameContext;
    //private Sprite tankSprite, bulletSprite, wallSprite;
    
    private int clientIdent = 0;		// идентификатор клиента, который будет присваиваться сервером
    
///------------------------ Объекты для тестирования -------------------------///
	private TankFactory testFactory = new TankFactory();
	
	private Tank testPlayerTank; 
	private Tank testTank2;
	
	private MapGenerator mapGen = new MapGenerator();
	private GameMap map;
   
//*--------------------------------------------------------------------------*//
	
    public ClientContext()
    {
    }
    
    @Override
    public void init(){
    	this.gameContext = new GameContext();
    	/*
    	testPlayerTank = (Tank) testFactory.createGameObject();
    	testTank2 = (Tank) testFactory.createGameObject();
    	
    	testPlayerTank.setWidth(32);
    	testTank2.setWidth(32);
    	testPlayerTank.setHeight(32);
    	testTank2.setHeight(32);
    	
    	testPlayerTank.setX(100);
    	testPlayerTank.setY(150);
    	
    	testTank2.setX(60);
    	testTank2.setY(60);
    	
    	testPlayerTank.setSprite(new Sprite(ResManager.getInstance().getImage(ResPathConsts.TANK_IMG_PATH), true));
    	testTank2.setSprite(new Sprite(ResManager.getInstance().getImage(ResPathConsts.TANK_IMG_PATH), true));
        
        testPlayerTank.setDirection(MoveDirection.DOWN);
        testTank2.setDirection(MoveDirection.RIGHT);
        
        try{
			map = mapGen.loadMapFromFile(ResPathConsts.MAP_DEFAULT_PATH, 32, 32);
			
	        ArrayList<MapTile> mapList = map.getTilesList();
	        for(MapTile tile : mapList){
	        	if(tile instanceof Wall) {
					tile.setSprite(new Sprite(ResManager.getInstance().getImage(ResPathConsts.TILE_WALL_IMG_PATH), false));
				}
	        	if(tile instanceof TileFloor) {
					tile.setSprite(new Sprite(ResManager.getInstance().getImage(ResPathConsts.TILE_FLOOR_IMG_PATH), false));
				}
	        }
		}
		catch(IOException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        testPlayerTank.setSpeed(2);
        testTank2.setSpeed(5);
        */
    }
    
    @Override
    public void dispose(){

    }
    
	
    @Override
    public void update(int dt){
    	
    	Map<String, ListWrapper> objectMap = gameContext.getContext();
        Set<String> keySet = objectMap.keySet();
        for(String k: keySet)
        {
            ArrayList<GameObject> lst = (ArrayList<GameObject>) objectMap.get(k).getList();
            for(GameObject go: lst)
            {
                if (k.equals(Tank.class.getCanonicalName()))
                {
                    Tank t = (Tank) go;
                    t.onMove();
                }
            }
        }
        
    	/*
    	ArrayList<MapTile> mapList = map.getTilesList();
    	boolean findCool = false;
    	for(MapTile tile : mapList){
    		if(tile.getType() == TileType.BLOCKER){
    			if(tile.checkColl(testPlayerTank)){
    				// если есть коллизия
    				findCool = true;
    				break;
    			}
    			else{
    				// если нет коллизии
    				
    			}
    		}
    	}
    	
    	if(!findCool){
    		testPlayerTank.onMove();
    	}
    	
        testTank2.onMove();
        */
    }

    @Override
    public void draw(Graphics2D g){
        
    	Map<String, ListWrapper> objectMap = gameContext.getContext();
        Set<String> keySet = objectMap.keySet();
        for(String k: keySet)
        {
            ArrayList<GameObject> lst = (ArrayList<GameObject>) objectMap.get(k).getList();
            for(GameObject go: lst)
            {
                if (k.equals(Tank.class.getCanonicalName()))
                {
                    Tank t = (Tank) go;
                    t.setSprite(new Sprite(ResManager.getInstance().getImage(ResPathConsts.TANK_IMG_PATH), true));
                    t.setDirection(t.getDirection());
                    t.onPaint(g);
                }
                if (k.equals(Wall.class.getCanonicalName()))
                {
                    Wall w = (Wall) go;
                    w.setSprite(new Sprite(ResManager.getInstance().getImage(ResPathConsts.TILE_WALL_IMG_PATH), false));
                    w.onPaint(g);
                }
                if (k.equals(TileFloor.class.getCanonicalName()))
                {
                    TileFloor tf = (TileFloor) go;
                    tf.setSprite(new Sprite(ResManager.getInstance().getImage(ResPathConsts.TILE_FLOOR_IMG_PATH), false));
                    tf.onPaint(g);
                }
                if (k.equals(Bullet.class.getCanonicalName()))
                {
                    Bullet b = (Bullet) go;
                    b.setSprite(new Sprite(ResManager.getInstance().getImage(ResPathConsts.TANK_IMG_PATH), true));
                    b.setDirection(b.getDirection());
                    b.onPaint(g);
                }
            }
        }
    	
        /*
        ArrayList<MapTile> mapList = map.getTilesList();
        for(MapTile tile : mapList){
        	tile.onPaint(g);
        }
        
    	// Рисуем тестовые танки
        testPlayerTank.onPaint(g);
        testTank2.onPaint(g);
        */
    }

    @Override
    public void keyPressed(KeyEvent e){
    	switch(e.getKeyCode()){
	    	case KeyEvent.VK_DOWN:{
	    		//testPlayerTank.setDirection(MoveDirection.DOWN);
	    		//testPlayerTank.setSpeed(4);
	    		break;
	    	}
	    	case KeyEvent.VK_UP:{
	    		//testPlayerTank.setDirection(MoveDirection.UP);
	    		//testPlayerTank.setSpeed(4);
	    		break;
	    	}
	    	case KeyEvent.VK_LEFT:{
	    		//testPlayerTank.setDirection(MoveDirection.LEFT);
	    		//testPlayerTank.setSpeed(4);
	    		break;
	    	}
	    	case KeyEvent.VK_RIGHT:{
	    		//testPlayerTank.setDirection(MoveDirection.RIGHT);
	    		//testPlayerTank.setSpeed(4);
	    		break;
	    	}
	    	case KeyEvent.VK_SPACE:{
	    		break;
	    	}
    	}
    }

    @Override
    public void keyReleased(KeyEvent e){
    	switch(e.getKeyCode()){
    	case KeyEvent.VK_DOWN:{
    		//testPlayerTank.setDirection(MoveDirection.DOWN);
    		//testPlayerTank.setSpeed(0);
    		break;
    	}
    	case KeyEvent.VK_UP:{
    		//testPlayerTank.setDirection(MoveDirection.UP);
    		//testPlayerTank.setSpeed(0);
    		break;
    	}
    	case KeyEvent.VK_LEFT:{
    		//testPlayerTank.setDirection(MoveDirection.LEFT);
    		//testPlayerTank.setSpeed(0);
    		break;
    	}
    	case KeyEvent.VK_RIGHT:{
    		//testPlayerTank.setDirection(MoveDirection.RIGHT);
    		//testPlayerTank.setSpeed(0);
    		break;
    	}
    	case KeyEvent.VK_SPACE:{
    		break;
    	}
	}
    }

    @Override
    public void keyTyped(KeyEvent e){
    }
    
    @Override
    public GameContext getGameContext() {
        return gameContext;
    }
    
    @Override
    public void setGameContext(GameContext gameContext) {
        this.gameContext = gameContext;
    }

}

package amm16_javabombert.core;

import java.awt.Graphics2D;
import java.awt.event.KeyListener;

/**
 *
 * Абстрактный класс состояния (стейта) игры.
 *
 */
public abstract class AppState implements KeyListener {

    private StateManager stateManager;

    public abstract void init();

    public abstract void dispose();

    public abstract void update(int dt);

    public abstract void draw(Graphics2D g);
    
    public abstract GameContext getGameContext();
    public abstract void setGameContext(GameContext gc);
    
    public static enum State {
        MAINMENU_STATE, // TODO: DEPRICATED
        CLIENT_STATE,
        SERVER_STATE
    }

    public StateManager getStateManager() {
        return stateManager;
    }

    public void setStateManager(StateManager stateManager) {
        this.stateManager = stateManager;
    }
}

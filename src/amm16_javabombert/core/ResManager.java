package amm16_javabombert.core;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import javax.imageio.ImageIO;

/**
 * 
 * Класс, через объекты которого происходит загрузка всех ресурсов
 * игры (картинки, звуки и т.д.)
 * Имеет необходимые методы для раздачи ресурсов.
 *
 */
public final class ResManager{
	
	private static volatile ResManager instance;
	private HashMap<String, Object> resources = new HashMap<String, Object>();
	
    private ResManager(){}
    
    public static ResManager getInstance(){
    	ResManager localInstance = instance;
        if(localInstance == null){
            synchronized (ResManager.class){
                localInstance = instance;
                if (localInstance == null){
                    instance = localInstance = new ResManager();
                }
            }
        }
        return localInstance;
    }

    /**
     *
     * @param key
     * @param t
     * @return
     * @throws ClassCastException при ошибке принудительного приведения типов
     */
    private Object get(String key) {
    	return resources.get(key);
    }
	
    public boolean load(String resPath, Class t){
        if(resources.get(resPath) == null){
            // загружаем ресурс
            if (t.equals(BufferedImage.class)){
                BufferedImage img = null;
                try {
                    img = ImageIO.read(new File(resPath));
                } catch (IOException e) { 
                    System.out.print(e.getMessage());
                }
                resources.put(resPath, img);
            }
        }
        // если ресурс уже загружен
        return false;
    }

    public boolean unload(String s){
            if(resources.remove(s) != null)
                    return true;
            return false;
    }

    public BufferedImage getImage(String s) {
            return (BufferedImage) get(s);
    }
}

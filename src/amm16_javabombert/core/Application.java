package amm16_javabombert.core;

import amm16_javabombert.entity.MapTile;
import amm16_javabombert.entity.MoveDirection;
import amm16_javabombert.entity.Tank;
import amm16_javabombert.entity.TankFactory;
import amm16_javabombert.entity.TileFloor;
import amm16_javabombert.entity.Wall;
import amm16_javabombert.map.GameMap;
import amm16_javabombert.map.MapGenerator;
import amm16_javabombert.network.NetworkManager;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * Основной класс-фасад игрового приложения. Содержит все ссылки на менеджеры
 * приложения.
 *
 */
public class Application implements Runnable {
	
	public static final boolean IS_DEBUG = true;
	
	public static final int MAP_TILES_WIDTH = 32;
	public static final int MAP_TILES_HEIGHT = 32;
	
    private StateManager stateManager;
    private NetworkManager networkManager;
    private int servNetworkMngPort = 10888;
    private ResManager resManager;

    private DrawZone contentPanel;
    private Graphics2D panelGfx;

    private boolean isAppRunning = false;
    private boolean isServer = false;
    private Thread myThread = null;
	private int FPS = 30;
	private int targetTime = 1000/FPS;
	private Timer loopTimer = null;
    
	private int pixTileSize;
	private int mapWidth;
	private int mapHeight;
	
	private GameContext DEBUG_testGC;
	
	/**
	 * 
	 * @param width ширина в тайлах
	 * @param height высота карты в тайлах
	 * @param pixTileSize размер тайла в пикселях
	 * @param contentPanel
	 */
    public Application(int width, int height, int pixTileSize, DrawZone contentPanel){
    	loopTimer = new Timer();
    	
    	// инициализация менеджеров приложения
        stateManager = StateManager.getInstance();
        stateManager.init();
        stateManager.setCurrentState(AppState.State.MAINMENU_STATE);
        resManager = ResManager.getInstance();
        networkManager = NetworkManager.getInstance();
        networkManager.setGameStateHandler();
        
        this.mapWidth = width;
        this.mapHeight = height;
        this.pixTileSize = pixTileSize;
        
        this.contentPanel = contentPanel;
        this.contentPanel.setSize(new Dimension(mapWidth * pixTileSize, mapHeight * pixTileSize));
        this.contentPanel.setDrawZone(mapWidth * pixTileSize, mapHeight * pixTileSize);
        this.contentPanel.setVisible(true);
    }

    public void changeState(AppState.State state) {
        stateManager.setCurrentState(state);
    }
    
    /**
     * Генерируем тестовый GameContext для тестов, отладки и дебагигна приложения.
     * @return
     */
    private GameContext DEBUG_createTestGameContext(){
    	GameContext gc = new GameContext();
    	
    	// load test default map
        try{
			GameMap map = new MapGenerator().loadMapFromFile(ResPathConsts.MAP_DEFAULT_PATH, MAP_TILES_WIDTH, MAP_TILES_HEIGHT);
			
	        ArrayList<MapTile> mapList = map.getTilesList();
	        for(MapTile tile : mapList){
	        	if(tile instanceof Wall) {
					tile.setSprite(new Sprite(ResManager.getInstance().getImage(ResPathConsts.TILE_WALL_IMG_PATH), false));
					gc.addGameObj(tile);
				}
	        	if(tile instanceof TileFloor) {
					tile.setSprite(new Sprite(ResManager.getInstance().getImage(ResPathConsts.TILE_FLOOR_IMG_PATH), false));
					gc.addGameObj(tile);
				}
	        }
		}
		catch(IOException e){
			e.printStackTrace();
		}
    	
        TankFactory tf = new TankFactory();
    	Tank testPlayerTank = (Tank) tf.createGameObject();
    	Tank testTank2 = (Tank) tf.createGameObject();
    	
    	testPlayerTank.setWidth(32);
    	testTank2.setWidth(32);
    	testPlayerTank.setHeight(32);
    	testTank2.setHeight(32);
    	
    	testPlayerTank.setX(100);
    	testPlayerTank.setY(150);
    	
    	testTank2.setX(60);
    	testTank2.setY(60);
    	
    	testPlayerTank.setSprite(new Sprite(ResManager.getInstance().getImage(ResPathConsts.TANK_IMG_PATH), true));
    	testTank2.setSprite(new Sprite(ResManager.getInstance().getImage(ResPathConsts.TANK_IMG_PATH), true));
        
        testPlayerTank.setDirection(MoveDirection.DOWN);
        testTank2.setDirection(MoveDirection.RIGHT);
        
        testPlayerTank.setSpeed(2);
        testTank2.setSpeed(5);
        
        gc.addGameObj(testTank2);
        gc.addGameObj(testPlayerTank);
        
    	return gc;
    }
    
    private GameContext initServerGameContext(){
    	GameContext gc = new GameContext();
    	
    	// грузим карту из файла и добавляем в GameContext
        try{
			GameMap map = new MapGenerator().loadMapFromFile(ResPathConsts.MAP_DEFAULT_PATH, MAP_TILES_WIDTH, MAP_TILES_HEIGHT);
			
	        ArrayList<MapTile> mapList = map.getTilesList();
	        for(MapTile tile : mapList){
	        	if(tile instanceof Wall) {
					gc.addGameObj(tile);
				}
	        	if(tile instanceof TileFloor) {
					gc.addGameObj(tile);
				}
	        }
		}
		catch(IOException e){
			e.printStackTrace();
		}
    	
    	return gc;
    }
    
    public void startApp(boolean isServer) {
        this.isServer = isServer;

        /*
    	 * Если не сервер, то загружаем ресурсы приложения.
    	 */
    	if(!isServer){
    		if(!loadResources()){
    			// ошибка при загрузке ресурсов
    		}
    		// For DEBUG
    		if(Application.IS_DEBUG)
    			DEBUG_testGC = DEBUG_createTestGameContext();

    		stateManager.setCurrentState(AppState.State.CLIENT_STATE);
    		
    		// For DEBUG
    		if(Application.IS_DEBUG)
    			((ClientContext) stateManager.getCurrentGameState()).setGameContext(DEBUG_testGC);
    	}
    	/*
    	 * Если сервер, то запускаем стейт ServerContext и запускаем NetworkManager
    	 */
    	else{
    		stateManager.setCurrentState(AppState.State.SERVER_STATE);
    		GameContext gc = initServerGameContext();		// создаем первый GameContext при запуске сервера
    		AppState state = stateManager.getCurrentGameState();
    		state.setGameContext(gc); 						// устанавливаем GameContext в стейт
    		
    		
    		System.out.println("App started as server on port " + servNetworkMngPort);
    	}
    	
    	isAppRunning = true;
    	if(myThread == null){
    		myThread = new Thread(this);
    		myThread.start();
    	}
    }

    /**
     * Загрузка всех необходимых ресурсов.
     *
     * @return
     */
    private boolean loadResources(){
    	boolean loadResult = true;
    	
    	if(resManager.load(ResPathConsts.TANK_IMG_PATH, BufferedImage.class) == false)
    		loadResult = false;
    	if(resManager.load(ResPathConsts.TILE_WALL_IMG_PATH, BufferedImage.class) == false)
    		loadResult = false;
    	if(resManager.load(ResPathConsts.TILE_FLOOR_IMG_PATH, BufferedImage.class) == false)
    		loadResult = false;
    	
    	return loadResult;
    }

    /**
     * Поток с главным игровым циклом, который ограничивается по FPS с помощью
     * Timer.
     */
    @Override
    public void run(){
    	int wait;
    	while(isAppRunning){
    		
    		// Обновляем таймер
    		loopTimer.update();
    		
    		// если сервер, то обновляем игру
    		// TODO: РАСКОМЕНТИТЬ УСЛОВИЕ! ЭТО ДЛЯ ТЕСТА
    		if(isServer)
    			update((int)loopTimer.deltaTimeMS);
    		
    		// если не сервер, то рисуем игру
    		if(!isServer){
    			panelGfx = this.contentPanel.getDrawZone().createGraphics();
    			panelGfx.fillRect(0, 0, mapWidth * pixTileSize, mapHeight * pixTileSize); // закрашиваем поле для перерисовки
    			draw(panelGfx);
    			contentPanel.repaint();
        		panelGfx.dispose();
    		}
    		
    		// For DEBUG
    		if(Application.IS_DEBUG && !isServer)
    			((ClientContext) stateManager.getCurrentGameState()).setGameContext(DEBUG_testGC);
    		
    		// формируем задержку для ограничения FPS
			wait = targetTime - loopTimer.currFPSDuration();
			if(wait > 0){
				try{
					Thread.sleep(wait);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
    	}
    }

    private void update(int dt) {
        stateManager.update(dt);
    }
    
    private void draw(Graphics2D gfx){
        stateManager.draw(gfx);
    }
	
	public StateManager getStateManager(){
		return stateManager;
	}
	public ResManager getResManager(){
		return resManager;
	}

	public int getServNetworkMngPort(){
		return servNetworkMngPort;
	}

	public void setServNetworkMngPort(int servNetworkMngPort){
		this.servNetworkMngPort = servNetworkMngPort;
	}
}

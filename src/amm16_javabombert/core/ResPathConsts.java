package amm16_javabombert.core;

/**
 *
 * Класс для объявления констант ресурсов игры (изображения, звуки, файлы).
 *
 */
public final class ResPathConsts{
	
	private static final String RES_DIR_PREFIX = "res/";
	
///--------------------------- Пути до файлов изображений ----------------------------///
	
	public static final String TANK_IMG_PATH = RES_DIR_PREFIX + "img/tank.png";
	public static final String TILE_WALL_IMG_PATH = RES_DIR_PREFIX + "img/tile_wall.png";
	public static final String TILE_FLOOR_IMG_PATH = RES_DIR_PREFIX + "img/tile_floor.png";

//*----------------------------------------------------------------------------------------------------------*//
	
///--------------------------- Пути до файлов карт ----------------------------///
	
	public static final String MAP_DEFAULT_PATH = RES_DIR_PREFIX + "maps/map_1.csv";

//*----------------------------------------------------------------------------------------------------------*//	
	
	private ResPathConsts(){}
}

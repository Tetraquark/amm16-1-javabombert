package amm16_javabombert.core;

import amm16_javabombert.entity.MoveDirection;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

public class Sprite {

    private BufferedImage currentFrame;
    private BufferedImage rotatedImages[];
    private int x;
    private int y;
    private boolean isRotatable;
    
    public Sprite(){
        
    }
    
    public Sprite(BufferedImage currentFrame, boolean isRotatable){
    	this.isRotatable = isRotatable;
        this.currentFrame = currentFrame;
        rotatedImages = new BufferedImage[4];
        rotatedImages[0] = currentFrame;
        
        if(isRotatable){
        	createRotateImgs();
        }
    }
    
    private void createRotateImgs(){
        for (int i = 1; i < 4; i++){
            AffineTransform tx = new AffineTransform();
            tx.quadrantRotate(1, rotatedImages[i-1].getWidth() / 2, rotatedImages[i-1].getHeight() / 2);
            AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
            rotatedImages[i] = op.filter(rotatedImages[i-1], null);
        }
    }
    
    public void setDirection(MoveDirection dir){
        if (null != dir)
            switch(dir){
                case DOWN:
                    currentFrame = rotatedImages[2];
                    break;
                case UP:
                    currentFrame = rotatedImages[0];
                    break;
                case LEFT:
                    currentFrame = rotatedImages[3];
                    break;
                case RIGHT:
                    currentFrame = rotatedImages[1];
                    break;
            }
    }

    public void setCoords(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public void drawSprite(Graphics2D gfx){
        gfx.drawImage(currentFrame, x, y, null);
    }
    
    public int getWidth(){
    	return currentFrame.getWidth();
    }
    
    public int getHeight(){
    	return currentFrame.getHeight();
    }
    
    public boolean isRotatable(){
    	return this.isRotatable;
    }
}

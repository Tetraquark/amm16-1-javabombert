/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amm16_javabombert.core;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
import javax.swing.JPanel;

/**
 *
 * @author tgrid
 */
public class DrawZone extends JPanel{
    private BufferedImage image;
    public void setDrawZone(int width, int height){
        if (image == null)
            image = new BufferedImage(width, height, TYPE_INT_ARGB);
    }
    public BufferedImage getDrawZone(){
       return image; 
    }
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);

        g.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
    }
}

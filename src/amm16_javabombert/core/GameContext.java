package amm16_javabombert.core;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import amm16_javabombert.entity.GameObject;

/**
 *
 * Класс-контейнер игрового контекста.
 *
 */
@XmlRootElement
public class GameContext {

    @XmlElementWrapper(name = "integerMap")
    public Map<String, ListWrapper> objectMap;

    public GameContext() {
        objectMap = new HashMap();
    }

    public String serialize() throws JAXBException {
        JAXBContext jc;
        StringWriter os;

        jc = JAXBContext.newInstance(GameContext.class);
        os = new StringWriter();
        jc.createMarshaller().marshal(this, os);

        return os.toString();
    }

    public GameContext deserialize(String s) throws JAXBException {
        GameContext de_context = new GameContext();
        StringReader sr = new StringReader(s);
        JAXBContext jc = JAXBContext.newInstance(GameContext.class);
        de_context = (GameContext) jc.createUnmarshaller().unmarshal(sr);

        return de_context;
    }

    public Map<String, ListWrapper> getContext()
    {
        return objectMap;
    }
    
    public void addGameObj(GameObject obj){
    	String objKey = obj.getClass().getCanonicalName();
    	if(objectMap.containsKey(objKey)){
    		ArrayList<GameObject> objsList = (ArrayList<GameObject>) objectMap.get(objKey).getList();
    		if(!objsList.contains(obj)){
    			objsList.add(obj);
    		}
    	}
    	else{
    		ArrayList<GameObject> newObjsList = new ArrayList<>();
    		newObjsList.add(obj);
    		ListWrapper lw = new ListWrapper(newObjsList);
    		objectMap.put(objKey, lw);
    	}
    }
}

package amm16_javabombert.core;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

import amm16_javabombert.entity.BulletFactory;
import amm16_javabombert.entity.GameObjectsFactory;
import amm16_javabombert.entity.TankFactory;
import amm16_javabombert.map.MapGenerator;
import java.util.ArrayList;
import java.util.List;

public class ServerContext extends AppState {

    private GameContext gameContext;
    private MapGenerator mapGenerator;

    private GameObjectsFactory tankFactory;
    private GameObjectsFactory bulletFactory;
    public List<ClientMsg> clientsMsg;

    public ServerContext() {
        //listener = new NetworkListener();
    }

    @Override
    public void init() {
        tankFactory = new TankFactory();
        bulletFactory = new BulletFactory();
        
    }

    @Override
    public void dispose(){

    }    

    @Override
    public void update(int dt) {
        clientsMsg = super.getStateManager().getClientMsgList();
        synchronized(clientsMsg){
            while(!clientsMsg.isEmpty()){
                ClientMsg message = clientsMsg.remove(clientsMsg.size()-1);
                System.out.println("a: " + message.a + " b: " + message.b + " c: " + message.c);
                // Далее надо парсить команды, пришедшие от сервера, протокол еще не утвержден
                // И обновлять гейм-контекст в соответствии в типом сообщения
                
            }
        }
    }

    @Override
    public void draw(Graphics2D g) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

	@Override
	public void keyPressed(KeyEvent e){
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e){
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e){
		// TODO Auto-generated method stub
		
	}
	
	@Override
    public GameContext getGameContext(){
        return gameContext;
    }

	@Override
	public void setGameContext(GameContext gc){
		this.gameContext = gc;
	}
    
}

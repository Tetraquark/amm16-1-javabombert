package amm16_javabombert.core;

/**
 *
 * Интерфейс для классов-держателей игрового контекста.
 * \\ Простите, какого контекста?
 */
public interface ContextHandler {

    public Object getContext();
}

package amm16_javabombert;

import javax.swing.JFrame;

import amm16_javabombert.core.GameContext;
import amm16_javabombert.core.ListWrapper;
import amm16_javabombert.core.AppMainFrame;
import amm16_javabombert.core.ClientMsg;
import amm16_javabombert.core.StateManager;
import amm16_javabombert.entity.Wall;

import amm16_javabombert.network.GameNetwork;
import amm16_javabombert.network.ClientAgent;
import amm16_javabombert.network.NetworkManager;
import java.io.IOException;

import java.io.StringReader;
import java.net.Socket;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.scene.input.KeyCode.X;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

/**
 *
 * Точка входа программы.
 *
 */
public class Amm16_javabombert {

    public static void main(String[] args) throws JAXBException {
        JFrame mainFrame = new AppMainFrame();
        mainFrame.setVisible(true);
    	
    	// Код для тестирования сетевого взаимодействия
    	/*
        if(args[0].startsWith("s"))
        {
            GameNetwork server = new GameNetwork();
            server.listenerThreadStart();
            
            GameContext context = new GameContext();
            for (Integer i = 0; i < 20; i++) {
                ListWrapper x;
                x = new ListWrapper();
                x.add(new Wall());
                x.add(new Wall());
                context.objectMap.put(i.toString(), x);
            }
            while (true)
                server.sendAll(context);
        }
        else
        {
            ClientAgent client = new ClientAgent("127.0.0.1", 10888, null);
            client.startThread();
            ClientMsg new_msg = new ClientMsg();
            new_msg.a = 100;
            new_msg.b = 200;
            for (int i = 0; i < 1; i++)
            {
                client.sendMsg(new_msg);
            }
            client.sendExit();
        }
        */
    }
}
